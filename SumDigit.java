public class SumDigit
{
public static void main(String[] args)
{
int n=12345; //initialising the int variable by giving some numbers to store it in n variable
int sum = 0;
while(n>0) //condition if n is greater than 0
 { 
int temp = n % 10; //taking the last digit
sum = sum + temp; //adding it in sum
n = n / 10; //remove the last digit
}
System.out.println(sum);
}
}
