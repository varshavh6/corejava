public class PowerFunc
{
public static void main(String[] args)
{
 //double keyword is use to store fractional number incase of just single number then will go with int variable 
double a = 2.5; //2.5 value will get stored in a variable 
double b = 3.7; //3.7 value will get stored in b variable
System.out.println(Math.pow(a,b)); //return the values of a and b
}
}